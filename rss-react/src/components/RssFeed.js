import React, { Component } from 'react';
// import { getFeed } from '../services/FeedService';

import Parser from 'rss-parser';

import './RssFeed.css';


class RssFeed extends Component {
    constructor(props) {
        super(props);

        this.state = {
            feed: null,
        };
    }

    async componentDidMount() {
        // get the feed
        let feed = await this.getFeed();
        console.log('form componentDidMount: ', feed);
    }

    getFeed() {
        // get rss feed
        let parser = new Parser();
        const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

        (async () => {
            let feed = await parser.parseURL(CORS_PROXY + "https://www.layerise.com/rss.xml");
            console.log('feed array: ', feed);
            this.setState({ feed: feed });
            console.log('from state', this.state.feed);
            return feed;
        })();
    }

    render() {
        return (
            <div className="rss-feed">
                RSS feeds:

                <div>{this.state.feed && this.state.feed.items.map((item) => {
                    return (
                        <div key={item.guid} className="feed-card">
                            <div className="feed-item-title">Title: {item.title}</div>
                            <div className="feed-item-pubdate">{item.pubDate}</div>
                            <div className="feed-item-link"><a href={item.link}>Read more...</a></div>
                            <div className="feed-item-content">{item.content}</div>

                        </div>

                    )
                })}</div>
            </div>

        );
    }
}


export default RssFeed;