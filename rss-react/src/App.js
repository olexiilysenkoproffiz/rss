import React, { Component } from 'react';
import './App.css';
import RssFeed from './components/RssFeed.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <RssFeed />
      </div>
    );
  }
}

export default App;
